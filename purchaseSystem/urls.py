"""purchaseSystem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from company import views
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'users'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^$', views.login),
    #url(r'^login/', views.login),
    #url(r'^login/',login,name='user_login'),
    #url(r'^logout/',logout,name='user_logout'),
    url(r'^login/',LoginView.as_view(), name='user_login'),
    url(r'^logout/',LogoutView.as_view(), name='user_logout'),
    url(r'^supplier/',views.supplier_inf),
    url(r'register/',views.register,name='user_register'),
    url(r'^purchase/',views.purchase),
    url(r'^check/',views.check),
    url(r'^quit/',views.quit),
    url(r'^pay/',views.pay),
    url(r'^judge/',views.judge),
    url(r'^judgePay/',views.judgePay),
    url(r'^supplierAdd/',views.supplierAdd),
    url(r'^supplierDelete/',views.supplierDelete),
    url(r'^invertory/',views.invertoryInf),
    url(r'^invertoryAdd/',views.invertoryAdd),
    url(r'^quitFromInv/',views.quitFromInv),
    url(r'^help/',views.help),
]
