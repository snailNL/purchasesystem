版本： 
	python 3.6.4
	django 1.11.13 update to django2.2.4 
    SQLite
	windows10 (或可用Ubuntu，centos)

##############################################################################
##############################################################################

整体逻辑框架(一)：用户采购过程
                                        用户登录(附加注册普通用户功能)
										   ||
										   ||
			 |-------------------------->主界面<---------------------------                           
			 |			(显示库存表链接，供应商信息表(手动增删))          |
			 |          (显示采购单，验货单，付款单，退货单链接)          |
			 |							   ||                             |
             |                             ||(开始采购)                   |
			 |							   ||                             |
			 |							采购界面------->(删除)------------|
			 |							   ||
			 |							   ||(填写采购信息并下单)
			 |							   ||
			 |                         跳到主界面
			 |						点击进入验货界面
			 |				       (提供验货单的填写)
			 |				               ||
			 |							   ||(提交验货单，并判断是否退货)
			 |							   ||
		 退货界面<---------(退货)--------判断框
             |                             ||  
             |                             ||(不退货，更新库存表，付款)
			 |							   ||
             |<----------(取消付款)-----付款界面
			 |				(显示采购信息，并提供付款单的填写)
			 |				               ||
			 |							   ||(提交付款单，并判断是否付款)
			 |							   ||
			 |<----------(取消付款,)-----判断框
			 |    (并通过库存渠道退货)     ||
			 |							   ||
			 |							   ||(确认付款)
			 |							   ||
             |<------------------------返回主界面


整体逻辑框架(二)：用户从库存中退货过程
                                        用户登录(附加注册普通用户功能)
										   ||
										   ||
			                             主界面                         
			 			(显示库存表链接，供应商信息表(手动增删))          
			            (显示采购单，验货单，付款单，退货单链接)          
			 							   ||                             
                                           ||(进入库存信息页面)                   
			 							   ||                             
			 							库存界面
			                      (显示供应商，采购链接)
			 			 (提供退货功能，增加库存货物种类功能)
			 							   ||
			 							   ||(点击进入“从库存中退货”)
			 							   ||
			                           跳到退货界面
			 				               ||
			 							   ||(提交退货单)
			 							   ||
		                       跳到主界面，库存货物信息减少
##############################################################################
##############################################################################									   
使用说明：
	1，项目名称为purchaseSystem
	2, windows系统下，打开cmd,进入该项目文件夹下
	3，输入命令 python manage.py runserver,启动服务器
	4，打开浏览器，输入http://127.0.0.1:8000/login/ 或 http://localhost:8000/login/进行登录操作
		后台管理员账号： 用户名: admin 密码： purchasesystem123
		一般用户账号： 用户名： lisi 密码： purchasesystem123(或点击注册按钮，注册一个用户)
	5，进入主界面（点击使用帮助，可以看到提供的各种操作以及其链接）
	6，按照逻辑框架进行操作即可。数据查询可以登录后台管理系统进行查看以及修改。
################################################################################
注意：
	1，后台管理登录地址为：127.0.0.1:8000/admin/
	   用户名：admin 密码： purchasesystem123
	2，后台管理模块可以查看和增改删各个数据表中的内容：
		supplier:供应商信息； purchase_order:采购单； payment_order:付款单；
		particular_order:验货单；Goods_Returned: 退货单；invertory：库存单
	3, 普通用户只提供增加和删除供应商的操作；修改供应商的操作交给后台管理员来进行
	4，对于库存信息，普通用户只提供增加库存货物种类的操作，删除库存和修改库存交给后台管理员来进行
	5，退货方式有两种： 一种采购过程中退货，库存信息不受影响；一种库存中退货，元库存商品数减少
	6，该系统目前为调试阶段，在./purchaseSystem/purchaseSystem/settings.py中， DEGBUG = True
	   (网页若出问题会显示问题症状)若改为False的话，就会跳转到使用模式，此时除了问题，会显示404。403啥的
		这个依据你那边的情况看着设置
	7，前端界面太烂。我CSS以及JavaScript技能树还未点亮。。。所以，就先整成这个样子。
	8，除了登录登出前端界面在./purchaseSystem/templates/registration中（下面有具体说明），
	其余所有的前端页面都在./purchaseSystem/templates/中

################################################################################
补充：
    1，增加采货单与验货单对比界面，即根据实际情况填完采货单后，会跳转到对比界面，此时再决定付款
		或者退货，对比的前端界面位于./purchaseSystem/templates/verifyProduct.html

################################################################################
	网页头部页面：./purchaseSystem/templates/header.html
	网页尾部页面：./purchaseSystem/templates/footer.html
    网页基本模板页面：./purchaseSystem/templates/base.html(这三个页面相当于一个通用的模板)
################################################################################
										   
用户登录实现：
	使用django内置登录登出模块：django.contrib.auth.views中的login与logout
	前端页面： ./purchaseSystem/templates/registration/login.html 和 /logout.html
	链接：http://127.0.0.1:8000/login/ 或 http://localhost:8000/login/
	
################################################################################	
用户注册实现：
	链接：http://127.0.0.1:8000/register/ 或 http://localhost:8000/register/
	实现的视图函数：./purchase/company/views.py 中的register
	表单格式：./purchase/company/form.py中的UserRegister
	前端页面： ./purchaseSystem/templates/register.html
	注意： 注册的用户为一般权限用户，后台管理用户为（用户名：admin; 密码： purchasesystem123）
	
################################################################################
供应商信息显示（主界面）(登录状态)：
	链接：http://127.0.0.1:8000/supplier/ 或 http://localhost:8000/supplier/
	实现的视图函数：./purchase/company/views.py 中的supplifierInf
	表单格式：./purchase/company/form.py中的SupplierMessage
	数据表格式：./purchase/company/mordels.py中的Supplier
	前端页面：./purchaseSystem/templates/supplier.html

################################################################################
增加供应商显示(登录状态)：
	链接：http://127.0.0.1:8000/supplierAdd/ 或 http://localhost:8000/supplierAdd/
	实现的视图函数：./purchase/company/views.py 中的supplierAdd
	表单格式：./purchase/company/form.py中的SupplierMessage
	数据表格式：./purchase/company/mordels.py中的Supplier
	前端页面：./purchaseSystem/templates/supplierAdd.html

################################################################################
删除供应商显示(登录状态)：
	链接：http://127.0.0.1:8000/supplierDelete/ 或 http://localhost:8000/supplierDelete/
	实现的视图函数：./purchase/company/views.py 中的supplierDelete
	表单格式：./purchase/company/form.py中的SupplierName
	前端页面：./purchaseSystem/templates/supplierDelete.html

################################################################################
显示库存(登录状态)：
	链接：http://127.0.0.1:8000/invertory/ 或 http://localhost:8000/invertory/
	实现的视图函数：./purchase/company/views.py 中的invertoryInf
	表单格式：./purchase/company/form.py中的Invertory
	数据表格式：./purchase/company/mordels.py中的invertory
	前端页面：./purchaseSystem/templates/invertory.html

################################################################################
增加库存商品种类(登录状态)：
	链接：http://127.0.0.1:8000/invertoryAdd/ 或 http://localhost:8000/invertoryAdd/
	实现的视图函数：./purchase/company/views.py 中的invertoryAdd
	表单格式：./purchase/company/form.py中的Invertory
	数据表格式：./purchase/company/mordels.py中的invertory
	前端页面：./purchaseSystem/templates/invertoryAdd.html
	
################################################################################
采购单(登录状态)：
	链接：http://127.0.0.1:8000/purchase/ 或 http://localhost:8000/purchase/
	实现的视图函数：./purchase/company/views.py 中的purchase
	表单格式：./purchase/company/form.py中的PurchaseOrder
	数据表格式：./purchase/company/mordels.py中的purchase_order
	前端页面：./purchaseSystem/templates/purchase.html

################################################################################
验货单(登录状态)：
	链接：http://127.0.0.1:8000/check/ 或 http://localhost:8000/check/
	实现的视图函数：./purchase/company/views.py 中的check
	表单格式：./purchase/company/form.py中的ParticularPaper
	数据表格式：./purchase/company/mordels.py中的particular_paper
	前端页面：./purchaseSystem/templates/check.html
	
################################################################################
付款单(登录状态)：
	链接：http://127.0.0.1:8000/pay/ 或 http://localhost:8000/pay/
	实现的视图函数：./purchase/company/views.py 中的pay
	表单格式：./purchase/company/form.py中的PaymentOrder
	数据表格式：./purchase/company/mordels.py中的payment_order
	前端页面：./purchaseSystem/templates/pay.html

################################################################################
退货单(登录状态，采购过程中退货，库存不变化)：
	链接：http://127.0.0.1:8000/quit/ 或 http://localhost:8000/quit/
	实现的视图函数：./purchase/company/views.py 中的quit
	表单格式：./purchase/company/form.py中的GoodsReturned
	数据表格式：./purchase/company/mordels.py中的goods_returned
	前端页面：./purchaseSystem/templates/quit.html
	
################################################################################
退货单(登录状态，在库存中退货，退货后库存数量变化)：
	链接：http://127.0.0.1:8000/quitFromInv/ 或 http://localhost:8000/quitFromInv/
	实现的视图函数：./purchase/company/views.py 中的quitFromInv
	表单格式：./purchase/company/form.py中的GoodsReturned
	数据表格式：./purchase/company/mordels.py中的goods_returned
	前端页面：./purchaseSystem/templates/quitFromInv.html

################################################################################
使用帮助：
	链接：http://127.0.0.1:8000/help/ 或 http://localhost:8000/help/
	实现的视图函数：./purchase/company/views.py 中的help
	前端页面：./purchaseSystem/templates/help.html
##############################################################################
##############################################################################		              
以用户登录注册过程为例，展示部分实现过程
（注意：此过程只是为了说明程序建造的步骤，原程序使用了django自带的登录模块）
（上述各个模块均可以使用此过程来完成）
一，创建项目
	django-admin startproject purchaseSystem ( cmd中输入命令，以下所有命令都在cmd中输入)

二，创建应用（app）
	进入purchaseSystem文件夹后，执行命令：
	python manage.py startapp company 

三，编写用户的数据模型类 Register
	进入company文件夹后，编译models.py:
	class Register(models.Model):
		username = models.CharField(max_length = 32,verbose_name = '用户名')
		password = models.CharField(max_length = 32,verbose_name = '密码')
		email = models.EmailField(verbose_name = '注册邮箱')
		phone_number = models.CharField(max_length = 14,verbose_name = '电话号码')

四，移动至purchaseSystem文件夹，执行命令：
	python manage.py makemigrations (创建了对应的数据库表: company_register)
	python manage.py migrate (创建数据库)
	
五，创建 form.py 表单提交格式，位于company文件夹下：
	class UserRegister(forms.Form):
		username = forms.CharField(label='注册用户名', max_length=100)
		password1 = forms.CharField(label='设置密码', widget=forms.PasswordInput())
		password2 = forms.CharField(label='确认密码', widget=forms.PasswordInput())
		email = forms.EmailField(label='电子邮件')
		phone_number = forms.CharField(label='手机号码',max_length=15)


	class UserLogin(forms.Form):
		username = forms.CharField(label='用户名', max_length=100)
		password = forms.CharField(label='密码', widget=forms.PasswordInput())
	
六，编写视图函数.company/views.py:
	def register(request):
		pass
	def login(request):
		pass

七，配置urls.py，用于网页的导向
	from django.conf.urls import url
	from django.contrib import admin
	from company import views

	urlpatterns = [
		url(r'^admin/', admin.site.urls),
		url(r'^$',views.login),
		url(r'^login/',views.login),
		url(r'^register/',views.register),
	]

八，编写登录页面以及注册页面的程序！
	1，在purchaseSystem文件夹下新建文件夹templates
	2, 在templates文件夹中写login.html,register.html,以及success.html
	3, 在templates文件夹中添加两个模块，admin和registration,这两个模块来自于
		./django/c ontrib/admin/templates/

九，配置./purchaseSystem/purchaseSystem文件夹下的settings.py文件
	1，在INSTALLED_APPS后添加应用 'company', 每新建一个应用，这个步骤必须执行
	2，在TEMPLATES中的DIR修改为： （该修改将新建的Templates设置为默认前端页面寻找地）
		'DIRS': [os.path.join(BASE_DIR,'templates'),], 
		下一行True改为False
	3，LANGUAGE_CODE = 'zh-hans' #en-us, 该项用于修改语言环境，改为中文。

十，创建超级用户（即后台管理员，可以进行增加删除等操作）
	python manage.py createsuperuser(根据要求设置即可，牢记)

十一，修改./compamy文件夹下admin.py，使超级用户可以直接管理注册或登录的用户
	添加：
	from .models import Register
	admin.site.register(Register)

十二，登录测试：
	urlpatterns = [
		url(r'^admin/', admin.site.urls),
		url(r'^$',views.login),
		url(r'^login/',views.login),
		url(r'^register/',views.register),
	]
	与上面的url相对应：http://127.0.0.1:8000/ 与 http://localhost:8000/等效
		1，http://127.0.0.1:8000/admin/ ,即后台管理员登录界面，可以进行增删等操作
		2，http://127.0.0.1:8000/ 或者 http://127.0.0.1:8000/login/ 进入登录界面 
		3，http://127.0.0.1:8000/register/ ,进入注册用户界面。
	点击登录后，页面转入success.html页面。
	
	至此，登录操作完成！
	
##############################################################################
##############################################################################

		