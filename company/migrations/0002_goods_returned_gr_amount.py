# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-19 08:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='goods_returned',
            name='gr_amount',
            field=models.IntegerField(null=True, verbose_name='数量'),
        ),
    ]
