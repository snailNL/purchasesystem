# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-18 08:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='goods_returned',
            fields=[
                ('gr_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='退货单号')),
                ('gr_salesReturn', models.CharField(max_length=100, verbose_name='待退货物')),
                ('gr_money', models.IntegerField(verbose_name='总价')),
                ('gr_reason', models.TextField(verbose_name='退货原因')),
                ('gr_buyerName', models.CharField(max_length=16, verbose_name='采购员')),
                ('gr_agreeOrNot', models.IntegerField(choices=[(0, '是'), (1, '否')], verbose_name='是否同意退货')),
            ],
        ),
        migrations.CreateModel(
            name='invertory',
            fields=[
                ('invertory_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='库存单号')),
                ('invertory_productList', models.CharField(max_length=100, verbose_name='货物名称')),
                ('invertory_amount', models.IntegerField(verbose_name='数量')),
            ],
        ),
        migrations.CreateModel(
            name='particular_paper',
            fields=[
                ('pp_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='验货单号')),
                ('pp_productList', models.CharField(max_length=100, verbose_name='货物名称')),
                ('pp_amount', models.IntegerField(verbose_name='数量')),
                ('pp_money', models.IntegerField(verbose_name='总价')),
                ('pp_report', models.TextField(verbose_name='验货报告')),
                ('pp_buyer', models.CharField(max_length=16, verbose_name='采购员')),
                ('pp_putIn', models.IntegerField(choices=[(0, '是'), (1, '否')], null=True, verbose_name='是否入库')),
            ],
        ),
        migrations.CreateModel(
            name='payment_order',
            fields=[
                ('pay_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='付款单号')),
                ('pay_tel', models.CharField(max_length=11, verbose_name='电话')),
                ('pay_bankCardNum', models.CharField(max_length=30, verbose_name='供应商银行卡号')),
                ('pay_moneyAmount', models.FloatField(verbose_name='待付金额')),
                ('pay_remark', models.TextField(null=True, verbose_name='备注')),
            ],
        ),
        migrations.CreateModel(
            name='purchase_order',
            fields=[
                ('po_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='单号')),
                ('po_buyer', models.CharField(max_length=16, verbose_name='采购员')),
                ('po_product', models.CharField(max_length=100, verbose_name='货物名称')),
                ('po_amount', models.IntegerField(verbose_name='数量')),
                ('po_money', models.FloatField(verbose_name='总价')),
                ('po_buyTime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='订单时间')),
                ('po_approvalStatus', models.IntegerField(choices=[(0, '通过'), (1, '未通过')], null=True, verbose_name='审批状态')),
                ('po_remark', models.TextField(null=True, verbose_name='备注')),
            ],
        ),
        migrations.CreateModel(
            name='Register',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=32, verbose_name='用户名')),
                ('password', models.CharField(max_length=32, verbose_name='密码')),
                ('email', models.EmailField(max_length=254, verbose_name='注册邮箱')),
                ('phone_number', models.CharField(max_length=14, verbose_name='电话号码')),
            ],
        ),
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('sup_name', models.CharField(max_length=16, primary_key=True, serialize=False, verbose_name='供应商姓名')),
                ('sup_phone', models.CharField(max_length=11, verbose_name='供应商电话号码')),
                ('sup_address', models.CharField(max_length=100, verbose_name='供应商地址')),
                ('sup_bank_number', models.CharField(max_length=30, verbose_name='供应商银行卡号')),
                ('sup_product', models.TextField(verbose_name='供应商货物信息')),
            ],
        ),
        migrations.AddField(
            model_name='purchase_order',
            name='po_sup',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Supplier', verbose_name='供应商'),
        ),
        migrations.AddField(
            model_name='payment_order',
            name='pay_sup',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Supplier', verbose_name='供应商'),
        ),
        migrations.AddField(
            model_name='particular_paper',
            name='pp_sup',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Supplier', verbose_name='供应商'),
        ),
        migrations.AddField(
            model_name='goods_returned',
            name='gr_sup',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Supplier', verbose_name='供应商名称'),
        ),
    ]
