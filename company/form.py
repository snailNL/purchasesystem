#coding:utf-8
from django import forms
from .models import purchase_order,particular_paper,invertory,goods_returned,payment_order,Supplier
from django.contrib.auth.models import User

# class UserRegister(forms.Form):
    #t_level = ((0,u'是'),(1,'b'),(2,'c'),)
    # username = forms.CharField(label='注册用户名', max_length=100)
    # password1 = forms.CharField(label='设置密码', widget=forms.PasswordInput())
    # password2 = forms.CharField(label='确认密码', widget=forms.PasswordInput())
    # email = forms.EmailField(label='电子邮件')
    # phone_number = forms.CharField(label='手机号码',max_length=15)
    #level = forms.ChoiceField(t_level,label='层级')

class UserRegister(forms.ModelForm):
    password1 = forms.CharField(label="Password",widget=forms.PasswordInput)
    password2 = forms.CharField(label="Confirm Password",widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ("username","email")
 
    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password1'] != cd['password2']:
            raise forms.ValidationError("Passwords do not match")
        return cd['password2']

class UserLogin(forms.Form):
    username = forms.CharField(label='用户名', max_length=100)
    password = forms.CharField(label='密码', widget=forms.PasswordInput())

class SupplierMessage(forms.ModelForm): #供应商
    class Meta:
        model = Supplier
        fields = ("sup_name","sup_phone","sup_address","sup_bank_number","sup_product")
    # sup_name = forms.CharField(max_length = 16, label = '供应商姓名')
    # sup_phone = forms.CharField(max_length = 11,label = '供应商电话号码')
    # sup_address = forms.CharField(max_length = 100,label = '供应商地址')
    # sup_bank_number = forms.CharField(max_length = 30,label = '供应商银行卡号')
    # sup_product = forms.CharField(max_length = 1000,label = '供应商货物信息')

class SupplierName(forms.Form):
    sup_name = forms.CharField(max_length = 16, label = '供应商姓名')


class Invertory(forms.ModelForm): #库存信息表
    class Meta:
        model = invertory
        fields = ("invertory_id","invertory_productList","invertory_amount")

class PurchaseOrder(forms.ModelForm): #采购单
    class Meta:
        model = purchase_order
        fields = ("po_id","po_buyer","po_sup","po_product","po_amount","po_money","po_buyTime","po_approvalStatus",
                  "po_remark")


class ParticularPaper(forms.ModelForm):#验货单
    class Meta:
        model = particular_paper
        fields = ("pp_id","pp_sup","pp_productList","pp_amount","pp_money","pp_report",
		           "pp_buyer","pp_putIn")

class GoodsReturned(forms.ModelForm):
    class Meta:
        model = goods_returned
        fields = ("gr_id","gr_sup","gr_salesReturn","gr_amount","gr_money","gr_reason","gr_buyerName","gr_agreeOrNot")

class PaymentOrder(forms.ModelForm):
    class Meta:
        model = payment_order
        fields = ("pay_id","pay_sup","pay_tel","pay_bankCardNum","pay_moneyAmount","pay_remark")