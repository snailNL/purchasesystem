#coding:utf-8
from django.shortcuts import render,render_to_response
import hashlib
from .models import Register,Supplier,invertory,purchase_order,particular_paper,goods_returned,payment_order
from django.template import RequestContext
from .form import UserLogin,UserRegister,SupplierMessage,Invertory,PurchaseOrder,ParticularPaper,GoodsReturned,PaymentOrder,SupplierName
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
#Create your views here.

#注册
def register(request):
    if request.method == 'POST':
        form = UserRegister(request.POST)
        if form.is_valid(): #获取表单信息
            username = form.cleaned_data['username']
            namefilter = Register.objects.filter(username = username)
            if len(namefilter) > 0:
                return render(request,'register.html',{'error':'用户名已存在'})
            else:
                # password1 = form.cleaned_data['password1']
                # password2 = form.cleaned_data['password2']
                # if password1 != password2:
                    # return render(request,'register.html',{'error':'两次输入的密码不一致！'})
                # else:
                    # password = password1
                    # email = form.cleaned_data['email']
                    # phone_number = form.cleaned_data['phone_number']
                    # #将表单写入数据库
                    # user = Register.objects.create(username=username,password=password,email=email,
                                                   # phone_number=phone_number)
                    # user.save()
                    # return render(request,'success.html',{'username':username,'operation':'注册'})
                new_user = form.save(commit=False)
                new_user.set_password(form.cleaned_data['password1'])
                #new_user.set_username(username)
                #new_user.set_email(form.cleaned_data['email'])
                new_user.save()
                return HttpResponseRedirect("/login/")
    else:
        form = UserRegister()
        return render(request,'register.html',{'form':form})


#登录
def login(request):
    if request.method == 'POST':
        form = UserLogin(request.POST)
        if form.is_valid(): #获取表单信息
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            #password = take_md5(password)
            namefilter = Register.objects.filter(username=username,password=password)
            if len(namefilter) > 0:
                return HttpResponseRedirect("/supplier/")
            else:
                return render(request,'login.html',{'error':'该用户名不存在！'})
    else:
        form =UserLogin()
        return render(request,'login.html',{'form':form})

#主界面设置
@login_required
def supplier_inf(request): #显示供应商信息
    sups = Supplier.objects.all()
    return render(request,"supplier.html",{"sups":sups})

#增加供应商
@login_required 
def supplierAdd(request):
    if request.method == 'POST':
        form = SupplierMessage(request.POST)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = Supplier.objects.create(
                          sup_name=form.cleaned_data["sup_name"],
                          sup_phone=form.cleaned_data["sup_phone"],
                          sup_address=form.cleaned_data["sup_address"],
                          sup_bank_number=form.cleaned_data["sup_bank_number"],
                          sup_product=form.cleaned_data["sup_product"],)

            user.save()
        return HttpResponseRedirect("/supplier/")
    else:
        form = SupplierMessage()
        return render(request,'supplierAdd.html',{'form':form})

#删除供应商
@login_required
def supplierDelete(request):
    if request.method == 'POST':
        del_company = SupplierName(request.POST)
        d = Supplier.objects.get(sup_name=request.POST.get("sup_name"))
        d.delete()
        return HttpResponseRedirect("/supplier/")
    else:
        form = SupplierName()
        return render(request,'supplierDelete.html',{'form':form})


@login_required
#采购单
def purchase(request):
    if request.method == 'POST':
        form = PurchaseOrder(request.POST)
        print(form)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = purchase_order.objects.create(po_id=form.cleaned_data["po_id"],
                        po_buyer=form.cleaned_data["po_buyer"],
                        po_sup=form.cleaned_data["po_sup"],
						po_product=form.cleaned_data["po_product"],
                        po_amount=form.cleaned_data["po_amount"],
                        po_money=form.cleaned_data["po_money"],
                        po_buyTime=form.cleaned_data["po_buyTime"],
                        po_approvalStatus=form.cleaned_data["po_approvalStatus"],
                        po_remark=form.cleaned_data["po_remark"])
            user.save()
        return HttpResponseRedirect("/supplier/")
    else:
        form = PurchaseOrder()
        return render(request,'purchase.html',{'form':form})

#设置验货函数
@login_required
def check(request):
    if request.method == 'POST':
        form = ParticularPaper(request.POST)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = particular_paper.objects.create(
                          pp_id=form.cleaned_data["pp_id"],
                          pp_sup=form.cleaned_data["pp_sup"],
                          pp_productList=form.cleaned_data["pp_productList"],
                          pp_amount=form.cleaned_data["pp_amount"],
                          pp_money=form.cleaned_data["pp_money"],
                          pp_report=form.cleaned_data["pp_report"],
                          pp_buyer=form.cleaned_data["pp_buyer"],
                          pp_putIn=form.cleaned_data["pp_putIn"])
            user.save()
            if form.cleaned_data["pp_putIn"] == 0:
                # inf = invertory.objects.create(invertory_id=form.cleaned_data["pp_id"],
                         # invertory_productList=form.cleaned_data["pp_productList"],
                         # invertory_amount=form.cleaned_data["pp_amount"],)
                inf = invertory.objects.get(invertory_productList=form.cleaned_data["pp_productList"])
                inf.invertory_amount += form.cleaned_data["pp_amount"]
                inf.save()
                po = purchase_order.objects.get(po_id=form.cleaned_data["pp_id"])
                pp = particular_paper.objects.get(pp_id=form.cleaned_data["pp_id"])
                return render(request,'verifyProduct.html',{'po':po,'pp':pp})
            else:
                return HttpResponseRedirect("/supplier/")
    else:
        form = ParticularPaper()
        return render(request,'check.html',{'form':form})

#查看库存信息
@login_required
def invertoryInf(request):
    products = invertory.objects.all()
    return render(request,"invertory.html",{"products":products})

#增加货物种类
@login_required
def invertoryAdd(request):
    if request.method == 'POST':
        form = Invertory(request.POST)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = invertory.objects.create(
                          invertory_id=form.cleaned_data["invertory_id"],
                          invertory_productList=form.cleaned_data["invertory_productList"],
                          invertory_amount=form.cleaned_data["invertory_amount"],)

            user.save()
            return HttpResponseRedirect("/supplier/")
    else:
        form = Invertory()
        return render(request,'invertoryAdd.html',{'form':form})

#判断是否退货
@login_required
def judge(request):
    return render(request,'judge.html')

#判断是否支付
@login_required
def judgePay(request):
    return render(request,'judgePay.html')

    

#退货函数（采购过程中退货）
@login_required
def quit(request):
    if request.method == 'POST':
        form = GoodsReturned(request.POST)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = goods_returned.objects.create(
                          gr_id=form.cleaned_data["gr_id"],
                          gr_sup=form.cleaned_data["gr_sup"],
                          gr_salesReturn=form.cleaned_data["gr_salesReturn"],
                          gr_amount=form.cleaned_data["gr_amount"],
                          gr_money=form.cleaned_data["gr_money"],
                          gr_reason=form.cleaned_data["gr_reason"],
                          gr_buyerName=form.cleaned_data["gr_buyerName"],
                          gr_agreeOrNot=form.cleaned_data["gr_agreeOrNot"])

            user.save()
            return HttpResponseRedirect("/supplier/")
    else:
        form = GoodsReturned()
        return render(request,'quit.html',{'form':form})
		

#从已入库的货物中退货
@login_required
def quitFromInv(request):
    if request.method == 'POST':
        form = GoodsReturned(request.POST)
        if form.is_valid(): #获取表单信息
            #判断是否有货
            pre_quit = invertory.objects.get(invertory_productList=form.cleaned_data["gr_salesReturn"])
            if pre_quit.invertory_id < 0:
                return HttpResponseRedirect("/quitFromInv/")
            elif pre_quit.invertory_amount - form.cleaned_data["gr_amount"] < 0:
                return HttpResponseRedirect("/quitFromInv/")
            else:    
                #将表单写入数据库
                pre_quit.invertory_amount -= form.cleaned_data["gr_amount"]
                pre_quit.save()
                user = goods_returned.objects.create(
                          gr_id=form.cleaned_data["gr_id"],
                          gr_sup=form.cleaned_data["gr_sup"],
                          gr_salesReturn=form.cleaned_data["gr_salesReturn"],
                          gr_amount=form.cleaned_data["gr_amount"],
                          gr_money=form.cleaned_data["gr_money"],
                          gr_reason=form.cleaned_data["gr_reason"],
                          gr_buyerName=form.cleaned_data["gr_buyerName"],
                          gr_agreeOrNot=form.cleaned_data["gr_agreeOrNot"])
                user.save()
            return HttpResponseRedirect("/invertory/")
    else:
        form = GoodsReturned()
        return render(request,'quitFromInv.html',{'form':form})



#付款函数
@login_required
def pay(request):
    if request.method == 'POST':
        form = PaymentOrder(request.POST)
        if form.is_valid(): #获取表单信息
            #将表单写入数据库
            user = payment_order.objects.create(
                          pay_id=form.cleaned_data["pay_id"],
                          pay_sup=form.cleaned_data["pay_sup"],
                          pay_tel=form.cleaned_data["pay_tel"],
                          pay_bankCardNum=form.cleaned_data["pay_bankCardNum"],
                          pay_moneyAmount=form.cleaned_data["pay_moneyAmount"],
                          pay_remark=form.cleaned_data["pay_remark"],)

            user.save()
        return HttpResponseRedirect("/judgePay/")
    else:
        form = PaymentOrder()
        return render(request,'pay.html',{'form':form})
def help(request):
    return render(request,'help.html')