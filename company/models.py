from django.db import models
from django.utils import timezone

# Create your models here.
class Register(models.Model):
    username = models.CharField(max_length = 32,verbose_name = '用户名')
    password = models.CharField(max_length = 32,verbose_name = '密码')
    email = models.EmailField(verbose_name = '注册邮箱')
    phone_number = models.CharField(max_length = 14,verbose_name = '电话号码')
    # t_level = ((0,u'是'),(1,'b'),(2,'c'),)
    # level = models.IntegerField(choices=t_level,verbose_name='层级', null=True)

    def  __str__(self):
        return self.username


class Supplier(models.Model): #供应商
    sup_name = models.CharField(max_length = 16, primary_key=True,verbose_name = '供应商姓名')
    sup_phone = models.CharField(max_length = 11,verbose_name = '供应商电话号码')
    sup_address = models.CharField(max_length = 100,verbose_name = '供应商地址')
    sup_bank_number = models.CharField(max_length = 30,verbose_name = '供应商银行卡号')
    sup_product = models.TextField(verbose_name = '供应商货物信息')

    def  __str__(self):
        return self.sup_name

class purchase_order(models.Model): #采购单
    status = ((0,u'通过'),(1,u'未通过'),)
    po_id = models.IntegerField(primary_key=True,verbose_name = '单号')
    po_buyer = models.CharField(max_length = 16,verbose_name = '采购员')
    po_sup = models.ForeignKey(Supplier,verbose_name = '供应商', on_delete=models.CASCADE)
    po_product = models.CharField(max_length = 100,verbose_name = '货物名称')
    po_amount = models.IntegerField(verbose_name = '数量')
    po_money = models.FloatField(verbose_name = '总价')
    po_buyTime = models.DateTimeField(default=timezone.now,verbose_name = '订单时间')
    po_approvalStatus = models.IntegerField(choices=status,verbose_name = '审批状态', null=True)
    po_remark = models.TextField(verbose_name = '备注', null=True)

    def  __str__(self):
        return str(self.po_id)+ ',' + self.po_product

class invertory(models.Model): #库存信息表
    invertory_id = models.IntegerField(primary_key=True,verbose_name = '库存单号')
    invertory_productList = models.CharField(max_length = 100,verbose_name = '货物名称')
    invertory_amount = models.IntegerField(verbose_name = '数量')

    def __str__(self):
        return str(self.invertory_productList)

class particular_paper(models.Model): #验货单
    judge = ((0,u'是'),(1,u'否'),)
    pp_id = models.IntegerField(primary_key=True,verbose_name = '验货单号')
    pp_sup = models.ForeignKey(Supplier,verbose_name = '供应商', on_delete=models.CASCADE)
    pp_productList = models.CharField(max_length = 100,verbose_name = '货物名称')
    pp_amount = models.IntegerField(verbose_name = '数量')
    pp_money = models.IntegerField(verbose_name = '总价')
    pp_report = models.TextField(verbose_name = '验货报告')
    pp_buyer = models.CharField(max_length = 16,verbose_name = '采购员')
    pp_putIn = models.IntegerField(choices=judge,verbose_name = '是否入库',null=True)

    def __str__(self):
        return str(self.pp_id)+', '+ str(self.pp_sup)

class goods_returned(models.Model): #退货单
    isAgree = ((0,u'是'),(1,u'否'),)
    gr_id = models.IntegerField(primary_key=True,verbose_name = '退货单号')
    gr_sup = models.ForeignKey(Supplier,verbose_name = '供应商名称', on_delete=models.CASCADE)
    gr_salesReturn = models.CharField(max_length = 100,verbose_name = '待退货物')
    gr_amount = models.IntegerField(verbose_name = '数量',null=True)
    gr_money = models.IntegerField(verbose_name = '总价')
    gr_reason = models.TextField(verbose_name = '退货原因')
    gr_buyerName = models.CharField(max_length=16,verbose_name = '采购员')
    gr_agreeOrNot = models.IntegerField(choices=isAgree,verbose_name = '是否同意退货')

    def __str__(self):
        return str(self.gr_sup)

class payment_order(models.Model): #付款单
    pay_id = models.IntegerField(primary_key=True,verbose_name = '付款单号')
    pay_sup = models.ForeignKey(Supplier,verbose_name = '供应商', on_delete=models.CASCADE)
    pay_tel = models.CharField(max_length = 11,verbose_name = '电话')
    pay_bankCardNum = models.CharField(max_length=30,verbose_name = '供应商银行卡号')
    pay_moneyAmount = models.FloatField(verbose_name = '待付金额')
    pay_remark = models.TextField(verbose_name = '备注', null=True)

    def __str__(self):
        return str(self.pay_id)+ ', '+str(self.pay_sup)
